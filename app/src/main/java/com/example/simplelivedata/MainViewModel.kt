package com.example.simplelivedata

import android.app.Application
import android.content.Context
import android.view.Menu
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel(context: Application) : AndroidViewModel(context) {

    private var _menuChart: MutableLiveData<List<SimpleChart>> = MutableLiveData(arrayListOf())
    val menuChart: LiveData<List<SimpleChart>> get() = _menuChart

    var amount: MutableLiveData<Int> = MutableLiveData(0)

    fun addToChart(menu: SimpleChart) {
        val arrayList = arrayListOf<SimpleChart>()
        arrayList.add(menu)
        _menuChart.value = arrayList
    }

}