package com.example.simplelivedata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.example.simplelivedata.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    private var isGrid = false
    private lateinit var sharedPreference: SharedPreference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        sharedPreference = SharedPreference(this)

        //button trigger
        /*
        * setiap kali button di klik, menuQuantity bertambah
        * klik ke 1 -> Android, 1
        * klik ke 2 -> Android, 2
        * klik ke 3 -> Android, 3
        * */

        /*viewModel.menuChart.observe(this) { data ->
            binding.btnAction.setOnClickListener {
                if (!data.isEmpty()) {
                    val quantity = data[0].menuQuantity + 1
                    viewModel.addToChart(SimpleChart("Android", quantity))
                } else {
                    viewModel.addToChart(SimpleChart("Android", 1))
                }
            }
            Log.e("SimpleLiveData", data.toString())
        }*/

        /*
        * setiap kali button di klik, object bertambah
        * klik ke 1 -> Android, 1
        * klik ke 2 -> Android, 1, Android, 2
        * klik ke 3 -> Android, 1, Android, 2, Android, 3
        */

        /*viewModel.menuChart.observe(this) { data ->
            binding.btnAction.setOnClickListener {
                if (!data.isEmpty()) {
                    data.forEach {
                        viewModel.addToChart(it)
                    }
                    val dataSize = data.size
                    val quantity = data[dataSize-1].menuQuantity + 1
                    viewModel.addToChart(SimpleChart("Android", quantity))
                } else {
                    viewModel.addToChart(SimpleChart("Android", 1))
                }
            }
        }*/
        //SEBELUM DI KLIK
        Log.e("IS_GRID", isGrid.toString())
        Log.e("IS_GRID_SHAREDPREF", sharedPreference.isGrid.toString())

        //SETELAH DI KLIK
        binding.btnActionChangeLayout.setOnClickListener {
            changeLayout(!isGrid)
            Log.e("IS_GRID", isGrid.toString())
            Log.e("IS_GRID_SHAREDPREF", sharedPreference.isGrid.toString())
        }
    }

    /*
    * isGrid = false
    * Ketika klik button, maka isGrid = true
    * apps ditutup, dan di buka lagi, isGrid = the latest condition (true)
    * */

    fun changeLayout(_isGrid: Boolean){
        this.isGrid = _isGrid
        sharedPreference.isGrid = _isGrid
    }
}