package com.example.simplelivedata

data class SimpleChart(
    var menuName: String,
    var menuQuantity: Int
)
